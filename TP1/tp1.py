import yaml
import argparse
import sys
import os

FICHIER_DE_CONFIGURATION = "configuration2.yaml"

parser = argparse.ArgumentParser()
parser.add_argument('--configuration', type=argparse.FileType('r', encoding='UTF-8'), required= False)




if len(sys.argv) == 2:
    file = sys.argv[1]
        
elif os.environ["FICHIER_DE_CONFIGURATION"]:
    file =  os.environ["FICHIER_DE_CONFIGURATION"]

else:
    file = FICHIER_DE_CONFIGURATION
   
        

with open(file) as fichier_config:
    config = yaml.safe_load(fichier_config)
    db = config["db"]
    print("db.url=", db["url"])
    print("db.user=", db["user"])
    print("db.password=", db["password"])




    